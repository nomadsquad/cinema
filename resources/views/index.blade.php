<html>

<head>
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.99.0/css/materialize.min.css">

    <style>
        #main_panel {

            width: 175vh;
            height: 90vh;

            position: absolute;
            top:0;
            bottom: 0;
            left: 0;
            right: 0;

            margin: auto;
        }

        tbody {
            display:block;
            height:50vh;
            overflow:auto;
        }
        thead, tbody tr {
            display:table;
            width:100%;
            table-layout:fixed;
        }
        thead {
            width: calc( 100% - 1em )
        }
        table {
            width:100%;
        }
    </style>

</head>

<body>
<meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="container">
        <div class="row">
            <div class="col s12 m12">
                <div class="card-panel deep-orange lighten-2 z-depth-5" id="main_panel">
                    <div class="col s12 m12">
                        <div class="center">
                            <button class="btn waves-effect grey darken-4" id="request_movies">Request Movies</button>
                            <button class="btn waves-effect grey darken-4 disabled" id="save_movies">Save Movies</button>
                            <button class="btn waves-effect grey darken-4" id="show_database">Show Database</button>

                            <br>
                            <p class="flow-text">The current date is: <span id="date"></span></p>

                            <div class="row" id="request_table">
                                <div class="card-panel grey lighten-5 z-depth-5" style="height: 65vh">
                                    <table class="responsive-table">
                                        <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Genre</th>
                                        </tr>
                                        </thead>

                                        <tbody id="tableContent">

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="row" id="database_table" style="display: none;">
                                <div class="card-panel grey lighten-5 z-depth-5" style="height: 65vh">
                                    <table class="responsive-table">
                                        <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Genre</th>
                                            <th>Created Date</th>
                                        </tr>
                                        </thead>

                                        <tbody id="databaseTableContent">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

<!-- Compiled and minified JavaScript -->
<script
        src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.99.0/js/materialize.min.js"></script>

<script>
    let data_array = [];

    $(document).ready(function () {
        $("#date").append(currentDate());

        $("#request_movies").click(function () {
            $.ajax({
                async: false,
                type: "POST",
                url: "https://api.themoviedb.org/3/discover/movie",
                data: {
                    api_key: "03d65b4c884b346a8088aa13f652312c",
                    "primary_release_date.gte": currentDate(),
                    "primary_release_date.lte": currentDate()
                },
                success: function(data){
                    data_array =  [];
                    $(data.results).each(function (index, value) {
                        data_array.push({title: value.title, genre: []});
                        $(value.genre_ids).each(function(genre_index, genre_value){
                            getGenre(index, genre_value);
                        })
                    });

                    showRequest(data_array);
                    Materialize.toast('Request finished with success', 4000)
                }
            });
        });

        $("#save_movies").click(function () {
            $.ajax({
                type: "POST",
                url: "saveMovies",
                data: {
                    '_token': $('meta[name="csrf-token"]').attr("content"),
                    movies: data_array
                },
                success: function(data){
                    Materialize.toast('Data saved successfully!', 4000)
                },
                error: function (err) {
                    Materialize.toast("Movies are already saved in the database", 4000)
                }
            });
        });

        $("#show_database").click(function () {
            $.ajax({
                type: "GET",
                url: "getMovies",
                data: {
                    '_token': $('meta[name="csrf-token"]').attr("content")
                },
                success: function(data){
                    showDatabase(data);
                }
            });
        })
    });

    function currentDate() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1;
        var yyyy = today.getFullYear();

        if(dd<10) {
            dd = '0'+dd
        }

        if(mm<10) {
            mm = '0'+mm
        }

        today = yyyy + '-' + mm + '-' + dd;
        return today;
    }

    function getGenre(index, id){
        $.ajax({
            async: false,
            type: "GET",
            url: "https://api.themoviedb.org/3/genre/"+ id +"?api_key=03d65b4c884b346a8088aa13f652312c",
            success: function(data){
                data_array[index].genre.push(data.name);
            }
        });
    }

    function showRequest(array){
        var id = "#tableContent";
        $(id).html("");
        $("#save_movies").removeClass("disabled");
        $("#request_table").show();
        $("#database_table").hide();

        $(array).each(function(index, value){
            $(id).append("<tr><td>"+ value.title +"</td><td>"+ value.genre.join("/") +"</td></tr>");
        });
    }

    function showDatabase(array){
        console.log(array);
        var id = "#databaseTableContent";
        $(id).html("");
        $("#save_movies").addClass("disabled");
        $("#request_table").hide();
        $("#database_table").show();

        $(array).each(function(index, value){
            $(id).append("<tr><td>"+ value.original_title +"</td><td>"+ value.genre+"</td><td>"+ value.created_at+"</td></tr>");
        });
    }
</script>


</html>
# Cinema Application

This application requests current information about the latest's movies for the current date.

I was unable to use [php-tmdb](https://github.com/php-tmdb/laravel) due to the lack of information about this wrapper, the bad documentation that it offers and it does not offers support for any custom search parameters.

Due to this issue:

```
cURL error 60: SSL certificate problem: unable to get local issuer certificate
```

This application must run with an apache virtual-host.
##Getting started

These instructions will help you install this project

You must install

* Composer
* XAMPP (Apache + MySql)
    * Php => 5.6
    * MySql =< 4.7

###Installing

**First you must create a new database named "cinema"**

Then in your console under this project folder you must enter 

```
$ composer install \\ To update your dependencies 

$ php artisan migrate \\ To migrate your table into your database
```

##Running the application

After installing XAMPP and migrate your table, run XAMPP Control Panel and go to:
```
http://localhost/cinema/public/
```
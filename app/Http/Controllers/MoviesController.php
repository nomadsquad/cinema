<?php

namespace App\Http\Controllers;

use App\Movies;
use Illuminate\Http\Request;
use PhpParser\Error;
use Tmdb\Laravel\Facades\Tmdb;
use Tmdb\Repository\MovieRepository;


class MoviesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        return Tmdb::getSearchApi()->searchMulti('jack');
//        return Tmdb::getConfigurationApi()->getConfiguration();
        $movies = Movies::all();

        return $movies;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $movies = $request->movies;

        $find_existent_movie = Movies::where('created_at', date("Y-m-d"))->first();

        if($find_existent_movie === NULL) {
            foreach ($movies as $movie) {
                $new_movie = new Movies();

                $new_movie->original_title = $movie['title'];
                $new_movie->genre = implode("/", $movie['genre']);
                $new_movie->created_at = date("Y-m-d");

                $new_movie->save();
            }
        } else {
            throw new Error("Movies are already saved in Database");
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Movies  $movies
     * @return \Illuminate\Http\Response
     */
    public function show(Movies $movies)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Movies  $movies
     * @return \Illuminate\Http\Response
     */
    public function edit(Movies $movies)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Movies  $movies
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Movies $movies)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Movies  $movies
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movies $movies)
    {
        //
    }
}
